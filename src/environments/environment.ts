// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCbLZb4c4g8skCN7li92bG2Wfjw8O4YJoA',
    authDomain: 'ng-bootstrap-auth-firebaseui.firebaseapp.com',
    databaseURL: 'https://ng-bootstrap-auth-firebaseui.firebaseio.com',
    projectId: 'ng-bootstrap-auth-firebaseui',
    storageBucket: 'ng-bootstrap-auth-firebaseui.appspot.com',
    messagingSenderId: '556721943788'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
