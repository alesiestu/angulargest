export const environment = {
  production: true,
  firebase: {
      apiKey: 'AIzaSyCbLZb4c4g8skCN7li92bG2Wfjw8O4YJoA',
      authDomain: 'ng-bootstrap-auth-firebaseui.firebaseapp.com',
      databaseURL: 'https://ng-bootstrap-auth-firebaseui.firebaseio.com',
      projectId: 'ng-bootstrap-auth-firebaseui',
      storageBucket: 'ng-bootstrap-auth-firebaseui.appspot.com',
      messagingSenderId: '556721943788'
  }
};
