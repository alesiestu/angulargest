import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class IngressoService {

  constructor(private firestore: AngularFirestore) {
  }

  getIngresso() {
    return this.firestore.collection('ingresso').snapshotChanges();
  }

}
